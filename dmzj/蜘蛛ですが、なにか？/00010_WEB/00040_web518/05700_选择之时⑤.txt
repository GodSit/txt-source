很短
全人类听见的爱丽儿的话语
======================

『啊～……。嗯！虽然开头有点丢人，不过那部份还请忘记，务必』

『好了。那接下来该说什么？主张？』

『主张，主张吗……』

『就算这样说，我也没什么想说的』

『因为，我压根没期待人类』

『这也当然，到这情况为止都还悠悠哉哉地生活著的家伙们，是有什么能期待的？』

『莎丽儿大人赌上性命保住了世界，而你们却忘记那份恩情至今』

『以为从那之后都过了多久的岁月？看看禁忌的履历就能大概明白了吧』

『嘛，虽然也有想刻意忘却而从历史抹消的这段事实的家伙在』

『而一～直旁观过来的我，早已经超过愤怒的地步，只剩下失望了』

『只在想，莎丽儿大人就是为了救这些家伙而牺牲的吗』

『人类过去也为了自己获救而选择牺牲莎丽儿大人。那这次会做出什么选择也已经相当明了了』

『所以我未曾期待，也不打算说服谁』

『但只有这个要先说』

『赢的，会是我们』

『如果谁也不去拯救莎丽儿大人——就连莎丽儿大人自身也一样——的话，那就由我来拯救莎丽儿大人』

『就算要牺牲半数以上的人类』

『如果你们想牺牲莎丽儿大人，那你们应该也有自己成为牺牲的觉悟吧？』

『因此，我要在这宣言』

『我，第二代魔王爱丽儿』

『作为魔族之王，不是系统所指定的伪物，而是真正的魔王』

『为了解放莎丽儿大人，我将继承初代魔王弗德伊抹杀全人类的遗志，在此对人类宣战』

『人类唷，为了女神去死吧』