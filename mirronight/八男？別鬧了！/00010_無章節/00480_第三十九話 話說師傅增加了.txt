﻿ 話說師傅增加了

「吶、露易絲的師傅是誰啊？」

「我也沒聽說、威爾呢?」

「我是有想到一個人但不知道該不該說。」

「哪個人？」

暑假期間終於結束、新學期卻和當初的預定變化很大。

已經從預備校畢業了不說、還被王都裡的大人們決定成人以前留在王都裡好好鍛鍊吧。

但也不能抱怨這個決定有什麼不好。

同樣是學習、王都的教育內容當然比較充實、基礎建設、娛樂設施也比較優良。

雖然沒有想沉溺在遊玩裡、但在休假的時候也能徹底放鬆了。

實際上在王都布萊希堡商業都市裡也玩的很開心。

總比那一點娛樂也沒有的老家還要好上很多。

住宿的房子也決定好了、聽說租金是布萊希萊德辺境伯爵出的。

『貴族間的爭奪戰真是恐怖。』不由得這麼想了、幾天之後、不知為何我與露易絲走向同一個地方。

埃爾文向近衛騎士的瓦倫先生學習劍術、伊娜也同樣向近衛騎士的槍術達人學習往城裡走去了，我和露易絲則是被叫到別的軍事營地。

我和露易絲都持有魔力、雖然她只能在魔闘流相關的技能上使用。

我是萬能型的魔法師、而另一種特化型魔法師也有一定數量存在。

對一種系統魔法有著驚人的天賦。

例如埃莉斯、就是只能使用以治癒為中心的聖屬性魔法。

另外還有、能製作魔道具卻不能使用魔法的人也有、不能使用生活魔法的人也有、能瞬間移動到遠方卻不能使用通訊魔法的也有。

通訊魔法屬於風屬性的魔法、能使用的魔法師都能從商人和軍隊手中大賺一筆。

如果是專家的話、就算隔了千里也能在沒有魔道具的情況下傳達訊息。

我應該也能使用通訊魔法但因為沒有可以遠距離傳話的對象所以傳送距離有多遠也不知道。

但是、如果對方不會通訊魔法或是持有高架的魔道具、通訊就無法成立、所以認為今後也沒那樣的機會。

「我和威爾要一起特訓嗎？」

「想學火焰魔法嗎？」

「那是不可能的事。」

露易絲是使用魔闘流的近戰特化型、我則是放出系的遠程戰鬥特化型。

因為這個理由、我和露易絲在同一的地方鍛鍊是不可能的。

實際上也會對彼此提出的訓練項目感到困擾。

「對不起、今天……」

「歐歐喔！終於來了啊！」

和營地的門番傳達目的之後立刻被帶入了、在進入某個建築物前、有一個曾經聽過的聲音向我們打招呼了。

那是曾經在暑假一起行動過半個月、在遠征魔務期間直接將魔物的肉烤來吃、跟宮廷首席魔導師比其來那人更適合當山賊、作為魔法師超一流、但不知為何在腦中只留下悶熱的形象。

與巨乳天使埃莉斯姿態完全無法聯想在一起的肌肉伯父。

克里姆特・克里斯托夫・馮・阿姆斯特朗子爵

(某文字君：我受不了了 作者你TM的逗我啊

為啥對一個大叔描述那麼多?!前面對後宮的形容詞加起來說不定都沒他多 你是想表明這位肌肉大叔才是正宮嗎((翻桌

那人還是一如往常用著悶熱的笑容來接待我們。

「阿姆斯特朗先生、就是你擔任我和威爾的老師嗎?」

「沒錯！昨天都興奮♂到睡不著了！！」

我正想破頭該如何從有一半以上的魔法師都想逃離的現實裡逃走——穿上用魔力具現化的鎧甲、甚至能徒手毆打龍的阿姆斯特朗先生成為我的導師了！！！

雖然對面很高興的樣子、但我這裡只有不好的預感。

或是說、我只想向阿姆斯特朗先生普通的學習罷了、絕對不是想學習如何徒手毆打龍的方法。

「(我才辦不到那種事情、或是說、那真的能稱為魔法嗎?。)」

可也因為那種魔法、阿姆斯特朗先生才能當上王宮首席魔導師吧、但我總覺得有種違和感。

「我怎麼可能學的會…..等等、露易絲的話也許學的會。」

我和阿姆斯特朗先生的魔闘流魔法有著很大的差別、但露易絲的相性就很好了。

在心中如此決定後、就向第一次見到王宮首席肌肉魔導師就讚不絕口的露易斯說：

「露易絲的話一定想參考那個魔法吧、我是不是不要妨礙你們比較好？」

「誒、只有我？威爾不是已經決定在一起嗎！」

我曾經向露易絲提問、阿姆斯特朗先生將魔力實體化製作了全身的魔導鎧甲、用高速的飛行魔法縱橫戰場、揮出的每一拳都能發出著高密度的魔力炮。那是否與魔闘流的技術相關、但她立刻就斷言魔闘流是沒有那種技術的。

『將魔力實體化？有那麼龐大的魔力誰會去學魔闘流啊。高密度的魔力炮、那也不可能是魔闘流的技能、魔闘流基本上是用少許的魔力附在手腳上使戰力大幅提升的技術、我雖然持有初級到中級之間的魔力、但關鍵的魔法沒辦法使用才去學魔闘流的。』

阿姆斯特朗先生的戰鬥方式有著壓倒性的攻擊力、但魔力也消耗的十分劇烈並不適合長時間的戰鬥。

但阿姆斯特朗先生戰鬥完後還是充滿精神、這也是他持有相當多魔力的證明吧。

真不愧是師傅也認可的人物呢。

但與清爽系的師傅相比、有點悶熱難受。

「不不、比起格鬥技、我比較適合用遠距離魔法掩護吧。」

劍術姑且是從小時候就開始練習了、但在預備校的時候被老師們宣判『完全沒有才能』。

在剛入學的時候多多少少有鍛鍊一下、但現在劍術的成績直線下降。

自鮑邁斯特的基礎訓練開始、我放棄劍術的時期比較晚罷了。

不過在弓術和投擲小刀方面倒是被說還挺有才能的、所以和魔法一起訓練了。

「我的劍術真的不行。」

「不會劍術的話那就換格鬥技好就沒關西了、一起學習嘛！」

不知為何露易絲拚命的想說服我、果然她也不想單獨和悶熱的王宮首席魔導師訓練啊。

我什麼我會知道、因為我也不想啊。

「我魔力的上限沒有達到、還需要作魔力的訓練。」

我還只有十二歲、每天都老實做著師傅教的魔力循環訓練。

「什麼、都已經持有那種程度的魔力了、還沒有達到上限嗎？」

「是、所以我……」

我原本打算犧牲露易絲自己就這麼回去了、但似乎沒那麼簡單。

阿姆斯特朗先生、留著眼淚一臉激動的看著我、雙手還緊緊的抓住我的肩膀。

「(肩膀好痛啊！骨頭要碎了！還有、逃不了了！)」

「如果是這樣的話、一起訓練就好了、用魔力循環訓練來學習魔導機動鎧甲是很有效率的、習慣了用強化過後的身體進行高速飛行戰鬥的話、就不需要魔闘流那種高層次的格鬥感了、也不需向俺學習武術。」

經過阿姆斯特朗先生的說明後、我喪失了逃走的理由。

倒不如說、這位肌肉大叔、用魔法創造出那種強悍、頑強的肉體、這世界的武術家看到、都會覺得這人太恐怖了。

「阿爾弗萊德、和只能使用魔闘流的俺不同、有著能使用各式各樣魔法的才能、但唯獨有一點令人惋惜、要不是他本人說了自己沒有才能、要不然俺也想教他一點魔導機動鎧甲的。」

說不定也不會在南方盡頭的森林裡喪命了。

阿姆斯特朗先生、用著寂寞的臉龐告訴我們。

「吶、威爾」

「也是呢、都還沒試過的事情、現在就說做不到還太早了。」

(某文字君：剛剛幼女苦苦哀求你留下你不要、現在肌肉大叔稍微流露出一點真情你就留了、其實你是基佬對吧威爾((跪倒

這次討伐的龍伴隨著第二面勛章、也沒辦法再用巧合來隱藏實力了、我如此想到。

但變的這麼顯眼、可能會使的各種麻煩接踵而至。

就算持有再多魔力的強大魔法師、也可能會發意外、在魔力不足時有數種防身的方法是在好不過了。

我就向阿姆斯特朗先生學習魔法格鬥術吧。

「少年喲、是擁有才能的、想必很快就會覺醒♂了。」

「非常感謝你、但是、真的沒問題嗎？」

我唯一擔心的一點就是、阿姆斯特朗先生是王功首席魔導師『不會很忙嗎?』

雖然無法想像阿姆斯特朗先生坐著文書工作並管理著部下、但肯定有一些那種工作吧。

「如果是那樣的話不必擔心、俺除了陛下呼叫以外是沒必要去城裡的。」

「誒？」

「你們試著想想看、王國是靠什麼力量統治至今日的？看上次的古雷特古蘭多討伐就很明顯了吧、王宮首席魔導士就是除了在緊急時刻以外有名無實的官位罷了。」

「但是、他也是個陛下、就算俺和他是從小就認識的朋友、也要定期露一下面。」

陛下的護衛也只需要幾名水平中等的王宮魔導士就足夠了、在那些中等的人中其他的夥伴負責與自己不同的文書處理、所以也沒什麼特別的問題、只要定期的在皇宮活動作為王宮首席魔導士露一下臉就行了。

但原來是這樣的啊、我認為阿姆斯特朗先生在街上看到絕對是令人不想回頭的類型、只是陛下剛好和在王宮裡被認為是五百年才出現一次人才的阿姆斯特朗先生是青梅竹馬。

如果他有那個心思、可以靠著魔法的才能爬上極高的地位。

對那些汲汲營營於權力鬥爭的貴族們、受到陛下喜愛的阿姆斯特朗先生是個極大的眼中釘吧。

『阿爾弗萊德比較適合作為王宮首席魔導士吧？』

為了與這種謠言保持距離、裝作愚者把麻煩的事情丟給部下、只在非常時期使用王宮首席魔導士的力量。

另一方面我的師傅、小時候做為孤兒在王都辛苦的度過了、為了避免成為王宮的餌食而逃往偏僻的南部了。

「(但這個人、是需要注意的人物吧)」

就算如此、陛下還是把它當作好友十分的信任他。

因為那份信賴、就算被利用俺也不會改變自己的決定。

沒辦法在放入任何東西了。

「啊、這個俺也有在訓練。」

「阿姆斯特朗先生的？」

「沒錯、俺的魔力界限也還沒有達到…..」

「誒———！」

明明現在就是妖怪了、阿姆斯特朗先生的魔力超過了四十歲還在成長中。

一般的話、二十歲就會達到魔力成長的界限。

也就是說阿姆斯特朗先生的成長力在魔法師中屬於異類。

「露易絲小姐、也還沒有到達魔力的上限嗎、那首先、進行今日最初的器合吧。」

結果那天、我和露易絲與阿姆斯特朗先生帶來的十幾名見習魔法師進行器合了。

器合就是將自己的最大魔力往對方傳輸、可能一次就達到對方的最大魔力量。

知道自己才能的底線、而無法接受這個事實對器合對象惡言相向的例子也有。

其實、雙方沒有信賴關係的話也無法進行器合。

就像是師徒關係之類的。

對這數十名承認阿姆斯特朗先生的弟子們來說、與魔力高的人進行器合自己的魔力也容易上升、想要與我們進行器合的傢伙就過來這裡。

當然、這種事情是不存在的。

說起來以前還有這種事情存在。

對一名有魔法才能的小嬰兒進行了器合、那個嬰兒獲得了龐大的魔力。

於是那個嬰兒每當哭泣的時候房子裡就充滿了風魔法。

想要喝母乳的時候、就用魔法強行的把母親拉到面前。

開始能行走後、就使用魔法搶走跟他一起遊玩的孩子們玩具。

所以進行器合時、必須要滿足兩個條件、對方一定要有自我控制力、併進行一定程度的魔法修行。

我是例外的樣子、阿姆斯特朗先生也抱持著『在五、六歲一般是不會進行器合的、小子持有的魔力實在是太多了、那會出問題。』的意見。

我因為心靈已經是個大叔了所以是例外吧。

「在這裡的全員、可能一次就到達魔力的界限了、與其為那數量感到悲傷、倒不如說你們已經幸運的節省了增加魔力量的時間、魔力量確實是很重要、但魔法還有很多需要鍛鍊的地方。」

不知道從哪裡搬過來床鋪讓與我進行器合被魔力醉倒的傢伙躺下、阿姆斯特朗先生則對他們進行說明。

但他們全員魔力最低也有著中等的水平、想必是將來的王宮魔導師後補。

「可是為什麼、阿姆斯特朗先生沒有被魔力醉倒？」

露易斯跟他們比起來、暈眩感比較少的樣子。

她在我的身邊坐下了、她的成長真的令人驚訝。

光是魔力量、就上升到將近上等的水平了。

出乎預料、不在顧慮家族進行魔力強化的訓練、開始展現出卓越的天賦。

但露易絲無法使用其他魔法的原因還是不明。

這是她今後的課題吧。

「那事因為、他和我一樣了呢。」

「那個….該不會？」

現在阿姆斯特朗的魔力和我完全一樣了。

而且和我一樣還沒到達魔力的界限。

換句話說、已經達到在師傅一倍以上了。

照這樣下去、師傅說不定會失去做為阿姆斯特朗先生競爭對手的資格。

「嗯、進行器合後大幅提升魔力的感覺真是久違了。多麼美妙的感覺啊…….那麼事不宜遲、趕快進行魔導機動鎧甲吧。」

「立刻就修行啊！」

「這不是當然的嗎！」

我和露易絲、對充滿精神的阿姆斯特朗先生感到無力了。

但那對今後的最強魔法鬥士傳說是沒有用的。
