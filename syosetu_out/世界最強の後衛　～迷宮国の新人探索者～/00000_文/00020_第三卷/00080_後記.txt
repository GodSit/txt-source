初次見面大家好，我是とーわ。雖然上一集也說過同樣的話，但我每天都在向支持至今的各位讀者表達衷心感謝。五體投地的感謝是我的基本姿勢。

相信各位讀者也有注意到，本作中的登場人物很多，因此在繪製插畫時，很難決定要讓哪個角色登場。通常都是依照責任編輯大人「這集要主推這個角色，硬要說的話」的指示選擇場景，再畫成插畫。※責任編輯的語氣其實並不是這種感覺，他對於作品的態度也相當一絲不苟，特別是要讓各位讀者拜見的內容，他絕對不會妥協──（字數要爆了）

──總之，我想說的是，我也希望登上插畫的角色能夠愈多愈好，但由於登場的人物非常多，希望各位讀者能在預測哪位角色會被畫成插畫的同時，從中得到樂趣。另外，關於劍道少女楓精明地爭取優勢的事，我很想知道各位讀者的想法。作者會一直等待各位的意見及感想。

篇幅也到了尾聲，請容我向負責插畫的風花風花老師、責任編輯大人、各位相關人員，以及最重要的讀者們再次獻上由衷的謝意。由力藏老師繪製的本作漫畫版也是，一併請各位讀者繼續多多支持了！期盼能於續集再度與各位相會。

とーわ